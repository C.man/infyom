<li class="{{ Request::is('projects*') ? 'active' : '' }}">
    <a href="{!! route('projects.index') !!}"><i class="fa fa-edit"></i><span>المشاريع</span></a>
</li>

<li class="{{ Request::is('assignments*') ? 'active' : '' }}">
    <a href="{!! route('assignments.index') !!}"><i class="fa fa-edit"></i><span>المهام</span></a>
</li>

<li class="{{ Request::is('cities*') ? 'active' : '' }}">
    <a href="{!! route('cities.index') !!}"><i class="fa fa-edit"></i><span>المدن</span></a>
</li>


<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>إدارة المستخدمين</span></a>
</li>

<li class="{{ Request::is('assignments*') ? 'active' : '' }}">
    <a href="{!! route('assignments.index') !!}"><i class="fa fa-edit"></i><span>Assignments</span></a>
</li>

