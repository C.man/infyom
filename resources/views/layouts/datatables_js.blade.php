<!-- Datatables -->
<script src="/datatables/jquery.dataTables.min.js"></script>
<script src="/datatables/dataTables.bootstrap.min.js"></script>
<script src="/datatables/dataTables.buttons.min.js"></script>
<script src="/datatables/buttons.bootstrap.min.js"></script>
<script src="/datatables/buttons.colVis.min.js"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>