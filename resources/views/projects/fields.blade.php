<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Piece Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('piece_number', 'Piece Number:') !!}
    {!! Form::text('piece_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Neighboring Field -->
<div class="form-group col-sm-6">
    {!! Form::label('neighboring', 'Neighboring:') !!}
    {!! Form::text('neighboring', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', 'City Id:') !!}
    {!! Form::select('city_id', $cities, null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner', 'Owner:') !!}
    {!! Form::text('owner', null, ['class' => 'form-control']) !!}
</div>

<!-- Work Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('work_owner', 'Work Owner:') !!}
    {!! Form::text('work_owner', null, ['class' => 'form-control']) !!}
</div>

<!-- Agreed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('agreed', 'Agreed:') !!}
    {!! Form::text('agreed', null, ['class' => 'form-control']) !!}
</div>

<!-- Received Field -->
<div class="form-group col-sm-6">
    {!! Form::label('received', 'Received:') !!}
    {!! Form::text('received', null, ['class' => 'form-control']) !!}
</div>


<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::date('start_date', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('projects.index') !!}" class="btn btn-default">Cancel</a>
</div>

<input type="hidden" name="remaining" value="0">
