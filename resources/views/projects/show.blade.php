 @extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Project
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @include('projects.show_fields')
                </div>
                <hr>
            </div>
        </div>
        <div>
            @foreach($project->assignments as $assignment)
             <!-- Default box -->
                <div class="box box-solid collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $assignment->name }}</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div style="display: none;" class="box-body">
                        <b>ID</b>: <code>{{ $assignment->id }}</code><br>
                        <b>Project Name</b>: <code>{{ $assignment->projects->name }}</code><br>
                        <b>Employee</b>: <code>{{ $assignment->employee }}</code><br>
                        <b>Duration</b>: <code>{{ $assignment->duration }}</code><br>
                        <b>Cost</b>: <code>{{ $assignment->cost }}</code><br>
                        <b>Assignment Case</b>: <code>{{ $assignment->case }}</code><br>
                        <b>Assignment Start In</b>: <code>{{ $assignment->start_in }}</code><br>
                        <b>Assignment End In</b>: <code>{{ $assignment->end_in }}</code><br>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->  
            @endforeach
        </div>
    </div>
@endsection
