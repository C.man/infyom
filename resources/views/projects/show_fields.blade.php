<div class="row">
    <div class="col-md-6">
        <!-- Id Field -->
        <div class="form-group">
            {!! Form::label('id', 'Id:') !!}
            <p>{!! $project->id !!}</p>
        </div>

        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            <p>{!! $project->name !!}</p>
        </div>

        <!-- Piece Number Field -->
        <div class="form-group">
            {!! Form::label('piece_number', 'Piece Number:') !!}
            <p>{!! $project->piece_number !!}</p>
        </div>

        <!-- Neighboring Field -->
        <div class="form-group">
            {!! Form::label('neighboring', 'Neighboring:') !!}
            <p>{!! $project->neighboring !!}</p>
        </div>

        <!-- City Id Field -->
        <div class="form-group">
            {!! Form::label('city_id', 'City Name:') !!}
            <p>{!! $project->cities->name !!}</p>
        </div>

        <!-- Owner Field -->
        <div class="form-group">
            {!! Form::label('owner', 'Owner:') !!}
            <p>{!! $project->owner !!}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Work Owner Field -->
        <div class="form-group">
            {!! Form::label('work_owner', 'Work Owner:') !!}
            <p>{!! $project->work_owner !!}</p>
        </div>

        <!-- Agreed Field -->
        <div class="form-group">
            {!! Form::label('agreed', 'Agreed:') !!}
            <p>{!! $project->agreed !!}</p>
        </div>

        <!-- Received Field -->
        <div class="form-group">
            {!! Form::label('received', 'Received:') !!}
            <p>{!! $project->received !!}</p>
        </div>


        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('start_date', 'Start Date:') !!}
            <p>{!! $project->start_date !!}</p>
        </div>

        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{!! $project->created_at !!}</p>
        </div>

        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{!! $project->updated_at !!}</p>
        </div>
    </div>
</div>