<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Employee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee', 'Employee:') !!}
    {!! Form::text('employee', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::text('cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Start In Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_in', 'Start In:') !!}
    {!! Form::date('start_in', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<!-- End In Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_in', 'End In:') !!}
    {!! Form::date('end_in', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<!-- Case Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case', 'Case:') !!}
    {!! Form::select('case', [0 => 'لم يبدأ', 1 => 'جاري', 2 => 'إنتهى'], null, ['class' => 'form-control']) !!}
</div>

<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project Id:') !!}
    {!! Form::select('project_id', $projects, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('assignments.index') !!}" class="btn btn-default">Cancel</a>
</div>
