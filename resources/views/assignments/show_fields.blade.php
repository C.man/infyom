<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $assignment->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $assignment->name !!}</p>
</div>

<!-- Employee Field -->
<div class="form-group">
    {!! Form::label('employee', 'Employee:') !!}
    <p>{!! $assignment->employee !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{!! $assignment->duration !!}</p>
</div>

<!-- Cost Field -->
<div class="form-group">
    {!! Form::label('cost', 'Cost:') !!}
    <p>{!! $assignment->cost !!}</p>
</div>

<!-- Start In Field -->
<div class="form-group">
    {!! Form::label('start_in', 'Start In:') !!}
    <p>{!! $assignment->start_in !!}</p>
</div>

<!-- End In Field -->
<div class="form-group">
    {!! Form::label('end_in', 'End In:') !!}
    <p>{!! $assignment->end_in !!}</p>
</div>

<!-- Case Field -->
<div class="form-group">
    {!! Form::label('case', 'Case:') !!}
    <p>{!! $assignment->case !!}</p>
</div>

<!-- Project Id Field -->
<div class="form-group">
    {!! Form::label('project_id', 'Project Id:') !!}
    <p>{!! $assignment->project_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $assignment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $assignment->updated_at !!}</p>
</div>

