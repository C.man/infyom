<?php

namespace App\Repositories;

use App\Models\Assignment;
use InfyOm\Generator\Common\BaseRepository;

class AssignmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'employee',
        'duration',
        'cost',
        'start_in',
        'end_in',
        'case'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Assignment::class;
    }
}
