<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Project;
use App\Models\Assignment;
use Illuminate\Http\Request;

class ArchiveController extends AppBaseController
{
	public function index()
	{
		$projects = Project::all();
		return view('arc.index', compact('projects'));
	}
}