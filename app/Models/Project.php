<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    public $table = 'projects';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'piece_number',
        'neighboring',
        'city_id',
        'owner',
        'work_owner',
        'agreed',
        'received',
        'remaining',
        'start_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'piece_number' => 'string',
        'neighboring' => 'string',
        'city_id' => 'integer',
        'owner' => 'string',
        'work_owner' => 'string',
        'agreed' => 'float',
        'received' => 'float',
        'remaining' => 'float',
        'start_date' => 'timestamp',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'piece_number' => 'required|string',
        'neighboring' => 'required|string',
        'owner' => 'required|string',
        'work_owner' => 'required|string',
        'agreed' => 'required|numeric',
        'received' => 'required|numeric',
        'start_date' => 'required',
    ];



    /**
     *  Get city assosiated with the related project.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cities()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }

    /**
     * Get assignments assosiated with the related project.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function assignments()
    {
        return $this->hasMany(\App\Models\Assignment::class, 'project_id', 'id');
    }

    public function getStartDateAttribute(){
        return $this->attributes['start_date'] = date('Y-m-d h:m:s');
    }

    // public function setRemainingAttribute()
    // {
    //     return $this->attributes['remaining'] = $this->agreed - $this->received;
    // }

   }
