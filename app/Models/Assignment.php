<?php

namespace App\Models;

use Eloquent as Model;

class Assignment extends Model
{

    public $table = 'assignments';
    


    public $fillable = [
        'name',
        'employee',
        'duration',
        'cost',
        'start_in',
        'end_in',
        'case',
        'project_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'employee' => 'string',
        'duration' => 'double',
        'cost' => 'double',
        'case' => 'string',
        'project_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'employee' => 'required|string',
        'duration' => 'required|numeric',
        'cost' => 'required|numeric',
        'start_in' => 'required',
        'end_in' => 'required',
        'case' => 'required|string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function projects()
    {
        return $this->belongsTo(\App\Models\Project::class, 'project_id', 'id');
    }

    /**
     * Description: This method return the numirc value of this attribute to string.
     * 
     * @return string
     */
    public function getCaseAttribute()
    {
        if ($this->attributes['case'] == 0) {
            return $this->attributes['case'] = 'لم يبدأ';
        } elseif ($this->attributes['case'] == 1) {
            return $this->attributes['case'] = 'جاري';
        }

        return $this->attributes['case'] = 'انتهى';
    }

    public function getDurationAttribute()
    {
        if ($this->attributes['duration'] <= 1) {
            return $this->attributes['duration'] . ' يوم';
        }

        return $this->attributes['duration'] . ' أيام';
    }
}
