<?php

namespace App\DataTables;

use App\Models\Assignment;
use Form;
use Yajra\Datatables\Services\DataTable;

class AssignmentDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'assignments.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $assignments = Assignment::query();

        return $this->applyScopes($assignments)->with('projects');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'employee' => ['name' => 'employee', 'data' => 'employee'],
            'duration' => ['name' => 'duration', 'data' => 'duration'],
            'cost' => ['name' => 'cost', 'data' => 'cost'],
            'start_in' => ['name' => 'start_in', 'data' => 'start_in'],
            'end_in' => ['name' => 'end_in', 'data' => 'end_in'],
            'case' => ['name' => 'case', 'data' => 'case'],
            'project_id' => ['name' => 'project_id', 'data' => 'projects.name']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'assignments';
    }
}
