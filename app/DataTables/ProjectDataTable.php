<?php

namespace App\DataTables;

use App\Models\Project;
use Form;
use Yajra\Datatables\Services\DataTable;

class ProjectDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'projects.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $projects = Project::query();

        return $this->applyScopes($projects)->with('cities');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'piece_number' => ['name' => 'piece_number', 'data' => 'piece_number'],
            'neighboring' => ['name' => 'neighboring', 'data' => 'neighboring'],
            'City' => ['name' => 'city_id', 'data' => 'cities.name'],
            'owner' => ['name' => 'owner', 'data' => 'owner'],
            'work_owner' => ['name' => 'work_owner', 'data' => 'work_owner'],
            'agreed' => ['name' => 'agreed', 'data' => 'agreed'],
            'received' => ['name' => 'received', 'data' => 'received'],
            'remaining' => ['name' => 'remaining', 'data' => 'remaining'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'projects';
    }
}
