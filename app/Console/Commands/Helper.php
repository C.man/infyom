<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Helper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:helper {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command create new helper to use it inside app directory.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $helperName = $this->argument('name');

        $content = app_path('Helpers\\Template\\') . 'tem.php';

        $filepath = app_path('Helpers\\') . $helperName . '.php';

        $file = file_get_contents($content);

        $file = str_replace("DammyText", $helperName, $file);
        
        file_put_contents($filepath, $file);
    }
}
