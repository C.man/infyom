
<?php

/**
 * This array return the default case to view: assignments.fields
 */
return [
    'case' => [
        'بدأ' => "بدأ",
        'جاري' => "جاري",
        'انتهى' => "انتهى",
    ]
];